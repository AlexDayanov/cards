//Constants
const cardsPath = "pics/Cards/";
const cardsNumber = 9;
const stackSize = cardsNumber * 2;
const suits = ['C', 'D', 'H', 'S'];
const kinds = ['0', '2', '3', '4', '5', '6', '7', '8', '9', 'J', 'Q', 'K', 'A'];

//Common variables
var stack = [];


function foo() {

    let bool = {
        index: 1,
        name: 2,
        imageUrl: 3,
        isOnTable: false,
        isReverted: false
    };
    console.log(bool.isOnTable = !bool.isOnTable);
}


function createStack() {
    let cardNames = getRandomCardNames();
    let indexes = getRandomIndexes();
    for (let i = 0; i < cardNames.length; i++) {
        stack.push(
            {
                index: indexes[i],
                name: cardNames[i],
                imageUrl: cardsPath + cardNames[i] + '.png',
                isOnTable: true,
                isReverted: false
            });
        stack.push(
            {
                index: indexes[stackSize-i-1],
                name: cardNames[i],
                imageUrl: cardsPath + cardNames[i] + '.png',
                isOnTable: true,
                isReverted: false
            });
    }
}

function getRandomCardNames() {
    let cardNames = [];
    let count = 0;
    while (count < cardsNumber) {
        let suitIndex = getRandomInt(0, suits.length);
        let kindIndex = getRandomInt(0, kinds.length);
        let cardName = kinds[kindIndex] + suits[suitIndex];
        if (!cardNames.includes(cardName)) {
            cardNames.push(cardName);
            count++;
        }
    }
    return cardNames;
}

function getRandomIndexes() {
    let indexes = [];
    let count = 0;
    while (count < stackSize) {
        let randomIndex = getRandomInt(0, stackSize);
        if (!indexes.includes(randomIndex)) {
            indexes.push(randomIndex);
            count++;
        }
    }
    return indexes;
}


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}