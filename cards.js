//  Когда я начинал писать этот код, только я и Бог могли разобраться в нем.
//  Теперь остался только Бог.. (с)


//Constants
const cardsPath = "pics/Cards/";
const jacketImage = "jacket.png";
const cardsNumber = 9;
const stackSize = cardsNumber * 2;
const suits = ['C', 'D', 'H', 'S'];
const kinds = ['0', '2', '3', '4', '5', '6', '7', '8', '9', 'J', 'Q', 'K', 'A'];
const initialRevertDelay = 5000;
const revertDelay = 1500;
const counterMultiplier = 42;
const gameContainer = document.getElementById('container');


//Common variables
let stack = [stackSize];
let openCards = [];
let scores = 0;
let starting = true;
let timerInitRevert;
let timerRevert;


//----------------------------------------------------------------------------------//


//Set update_game click eventListener
document.querySelector('#header > a').addEventListener('click', render);

//Initially renders the cards stack over the table
function render() {
    starting = true;
    openCards.length = 0;
    stopTimer(timerInitRevert);
    createShuffledStack(cardsNumber);

    // Set scores to zero
    scores = 0;
    document.getElementById('score1').innerText = scores;

    //Define parent element (container)
    gameContainer.innerHTML = '';
    gameContainer.classList.remove('start', 'end');
    gameContainer.classList.add('game');

    //Set header visible
    document.getElementById('header').classList.remove('hidden');

    //Iterate over the cards stack
    stack.forEach(function (card) {

        //Virtually create DOM-element <div>
        let cardDiv = document.createElement('div');

        //Set DOM-element properties
        cardDiv.className = "card";
        cardDiv.style.setProperty('--pictureUrl', "url(" + card.imageUrl + ")");
        cardDiv.dataset.index = card.index;
        cardDiv.dataset.tid = "Card";

        //Add event listener to the DOM-element
        cardDiv.onclick = function (event) {
            let index = event.target.dataset.index;
            if (stack[index].isReverted) {
                if (openCards.length < 2) {
                    openCards.push(index);
                    turnCards([event.target]);
                }
                if (openCards.length === 2) {
                    timerRevert = window.setTimeout(arbiter, revertDelay)
                }
            }
        };

        //Append newly created div to the parent element
        gameContainer.appendChild(cardDiv);
    });

    //Revert the whole stack
    timerInitRevert = window.setTimeout(() => {
            if (starting) turnCards(Array.from(document.getElementsByClassName("card")));
            starting = false;
        }, initialRevertDelay
    );
}

//Set up game end screen
function finishGame() {
    document.getElementById('header').classList.add('hidden');
    gameContainer.classList.add('start', 'end');
    gameContainer.innerHTML = '<div id="end-msg">Поздравляем!<br />Ваш итоговый счет:' + "  " + +scores + '</div>' +
        '<button data-tid="EndGame-retryGame" id="butt" onclick="render()">Еще раз</button>'
}

//Manages cards being clicked
function arbiter() {

    if (openCards.length !== 2) return;
    let result = false;
    const [index1, index2] = openCards;
    const cardElement1 = document.querySelector('[data-index="' + index1 + '"]');
    const cardElement2 = document.querySelector('[data-index="' + index2 + '"]');

    if (stack[index1].name === stack[index2].name) {

        stack[index1].isOnTable = stack[index2].isOnTable = false;
        cardElement1.classList.add('hidden');
        cardElement2.classList.add('hidden');

        result = true;
    } else {
        turnCards([cardElement1, cardElement2]);
    }
    openCards = [];
    scoreCounter(result);
}


//Counts scores and initiates game end
function scoreCounter(result) {
    let cardsCount = 0;
    for (let card of stack) {
        if (result) card.isOnTable && cardsCount++;
        else !card.isOnTable && cardsCount--;
    }
    scores += (counterMultiplier / 2 * cardsCount);
    document.getElementById('score1').innerText = scores;
    if (result && cardsCount === 0) finishGame()
}


//Turns cards over
function turnCards(cardsCollection) {

    cardsCollection
        .forEach(cardEl => {
            let index = cardEl.dataset.index;

            if (stack[index].isReverted) {
                cardEl.style.setProperty('--pictureUrl', "url(" + stack[index].imageUrl + ")");

            } else if (openCards.length <= 2) {
                cardEl.style.setProperty('--pictureUrl', "url(" + cardsPath + jacketImage + ")");

            }

            stack[index].isReverted = !stack[index].isReverted;
            document.querySelector('[data-index="' + index + '"]').dataset.tid = stack[index].isReverted ? 'Card-flipped' : 'Card'

        });
}


// Builds randomly cards stack
function createShuffledStack() {
    stack = [];
    let cardNames = getRandomCardNames();
    let indexes = getRandomIndexes();
    for (let i = 0; i < cardNames.length; i++) {
        stack[indexes[i]] =
            {
                index: indexes[i],
                name: cardNames[i],
                imageUrl: cardsPath + cardNames[i] + '.png',
                isOnTable: true,
                isReverted: false
            };
        stack[indexes[stackSize - i - 1]] =
            {
                index: indexes[stackSize - i - 1],
                name: cardNames[i],
                imageUrl: cardsPath + cardNames[i] + '.png',
                isOnTable: true,
                isReverted: false
            };
    }
}


//Generate random cards' names
function getRandomCardNames() {
    let cardNames = [];
    let count = 0;
    while (count < cardsNumber) {
        let suitIndex = getRandomInt(0, suits.length);
        let kindIndex = getRandomInt(0, kinds.length);
        let cardName = kinds[kindIndex] + suits[suitIndex];
        if (!cardNames.includes(cardName)) {
            cardNames.push(cardName);
            count++;
        }
    }
    return cardNames;
}


//Generate random cards' indexes
function getRandomIndexes() {
    let indexes = [];
    let count = 0;
    while (count < stackSize) {
        let randomIndex = getRandomInt(0, stackSize);
        if (!indexes.includes(randomIndex)) {
            indexes.push(randomIndex);
            count++;
        }
    }
    return indexes;
}


//Random range-Int generator
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

function stopTimer(timer) {
    window.clearTimeout(timer);
    console.log('cleared')
}